<?php

namespace JontyNewman\EMF;

interface Repository
{
	public function fetch(string $id): ?Entity;

	public function store(string $id, Entity $entity): void;
}
