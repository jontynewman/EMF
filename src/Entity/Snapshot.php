<?php

namespace JontyNewman\EMF\Entity;

use JontyNewman\EMF\Entity;
use JontyNewman\EMF\Entity\Binary;
use JontyNewman\EMF\Entity\Touchable;
use RuntimeException;

class Snapshot implements Entity
{
	const MESSAGE = 'Cannot start output buffer';

	use Binary, Touchable;

	private $binary;

	public static function buffer(
			Entity $entity,
			int $start = 0,
			int $length = null
	): string {

		$string = '';

		try {
			ob_start();
			$entity->render($start, $length);
		} finally {
			$string .= (string) ob_get_clean();
		}

		return $string;
	}

	public static function capture(Entity $entity): Entity
	{
		return new self($entity);
	}

	protected function binary(): string
	{
		return $this->binary;
	}

	private function __construct(Entity $entity)
	{
		$this->binary = self::buffer($entity);
		$this->touch();
	}
}
