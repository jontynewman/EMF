<?php

namespace JontyNewman\EMF\Entity;

use InvalidArgumentException as Exception;
use JontyNewman\EMF\Entity\UnsupportedRangeError as Error;

trait Unranged
{
	public function render(int $start = 0, int $length = null): void
	{
		// If the range is not full...
		if (!is_null($length) || $start != 0) {
			throw new class extends Exception implements Error {};
		}

		$this->passthru();
	}

	protected abstract function passthru();
}
