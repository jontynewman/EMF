<?php

namespace JontyNewman\EMF\Entity;

use DateTime;
use SplFileInfo;
use SplFileObject;

trait File
{
	public function modified(): DateTime
	{
		$info = new SplFileInfo($this->path());
		return DateTime::createFromFormat('U', $info->getMTime());
	}

	public function render(int $start = 0, int $length = null): void
	{
		$file = new SplFileObject($this->path(), 'rb');

		$file->fseek($start);

		if (is_null($length)) {
			$file->fpassthru();
		} else {
			$this->partial($file, $length);
		}
	}

	protected abstract function path(): string;

	protected function buffer(): int
	{
		return 0x1000;
	}

	private function partial(SplFileObject $file, int $length): void
	{
		$rendered = 0;

		while (!$file->eof() && $rendered < $length) {
			$buffer = $this->buffer();
			echo (string) substr($file->fread($buffer), 0, $length - $rendered);
			$rendered += $buffer;
		}
	}
}
