<?php

namespace JontyNewman\EMF\Entity;

use DateTime;

trait Touchable
{
	private $touched = null;

	public function modified(): DateTime
	{
		return $this->touched ?? $this->now();
	}

	protected function touch(): void
	{
		$this->touched = $this->now();
	}

	private function now(): DateTime
	{
		return $now = new DateTime;
	}
}
