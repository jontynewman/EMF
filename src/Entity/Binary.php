<?php

namespace JontyNewman\EMF\Entity;

use JontyNewman\EMF\Range;

trait Binary
{
	public function render(int $start = 0, int $length = null): void
	{
		if (is_null($length)) {
			echo (string) substr($this->binary(), $start);
		} else {
			echo (string) substr($this->binary(), $start, $length);
		}
	}

	protected abstract function binary(): string;
}
