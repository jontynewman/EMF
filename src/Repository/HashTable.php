<?php

namespace JontyNewman\EMF\Repository;

use JontyNewman\EMF\Entity;

trait HashTable
{
	private $entities = [];

	public function fetch(string $id): ?Entity
	{
		$entity = $this->entities[$id] ?? null;

		return $entity;
	}

	public function store(string $id, Entity $entity): void
	{
		$limit = $this->limit();
		$this->entities[$id] = $entity;

		if (!is_null($limit) && count($this->entities) > $limit) {
			array_shift($this->entities);
		}
	}

	protected function limit(): ?int
	{
		return null;
	}
}
