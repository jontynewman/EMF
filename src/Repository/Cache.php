<?php

namespace JontyNewman\EMF\Repository;

use JontyNewman\EMF\Entity;
use JontyNewman\EMF\Entity\Snapshot;
use JontyNewman\EMF\Repository;
use JontyNewman\EMF\Repository\HashTable;

trait Cache
{
	use HashTable {
		HashTable::fetch as get;
		HashTable::store as set;
	}

	public function fetch(string $id): ?Entity
	{
		$cached = $this->get($id);

		if (is_null($cached)) {
			$entity = $this->repository()->fetch($id);
		}

		if (is_null($cached) && !is_null($entity)) {
			$this->set($id, Snapshot::capture($entity));
		}

		return $cached ?? $entity;
	}

	public function store(string $id, Entity $entity): void
	{
		$this->repository()->store($id, $entity);
	}

	protected abstract function repository(): Repository;

	protected function cache(string $id): bool
	{
		$expired = false;
		$entity = $this->repository()->fetch($id);

		if (!is_null($entity)) {

			$cache = $this->get($id);

			if (is_null($cache) || $entity->modified() >= $cache->modified()) {

				$this->set($id, Snapshot::capture($entity));
				$expired = true;
			}
		}

		return $expired;
	}
}
