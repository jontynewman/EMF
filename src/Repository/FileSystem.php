<?php

namespace JontyNewman\EMF\Repository;

use JontyNewman\EMF\Entity;
use JontyNewman\EMF\Entity\File;
use SplFileInfo;
use SplFileObject;

trait FileSystem
{
	public function fetch(string $id): ?Entity
	{
		$entity = null;
		$info = new SplFileInfo($id);

		if ($info->isFile()) {
			$entity = $this->toEntity($info);
		}

		return $entity;
	}

	public function store(string $id, Entity $entity): void
	{
		$file = new SplFileObject($id, 'wb');
		ob_start(function (string $buffer) use ($file) {
			$file->fwrite($buffer);
			return '';
		}, $this->buffer());
		$entity->render();
		ob_end_clean();
	}

	protected function buffer(): int
	{
		return 0x1000;
	}

	private function toEntity(string $path): Entity
	{
		return new class($path) implements Entity {

			use File;

			private $path;

			public function __construct(string $path)
			{
				$this->path = $path;
			}

			protected function path(): string
			{
				return $this->path;
			}
		};
	}
}
