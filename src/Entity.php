<?php

namespace JontyNewman\EMF;

use DateTime;

interface Entity
{
	public function modified(): DateTime;

	public function render(int $start = 0, int $length = null): void;
}
