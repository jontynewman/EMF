<?php

namespace JontyNewman\EMF\Tests;

use JontyNewman\EMF\Entity;
use JontyNewman\EMF\Entity\Snapshot;
use JontyNewman\EMF\Entity\Touchable;
use JontyNewman\EMF\Entity\Unranged;
use JontyNewman\EMF\Entity\UnsupportedRangeError;
use JontyNewman\EMF\Repository;
use JontyNewman\EMF\Repository\Cache;
use JontyNewman\EMF\Repository\FileSystem;
use org\bovigo\vfs\vfsStream;
use PHPUnit\Framework\TestCase;
use RuntimeException;

class Test extends TestCase
{
	const BUFFER = 'Goodbye, galaxy!';

	public function test()
	{
		$error = null;
		$root = vfsStream::setup();
		$path = "{$root->url()}/file";
		$entity = $this->unranged();
		$repo = $this->repository($entity);

		// Assert that entities can be fetched.
		$this->assertSame($entity, $repo->fetch($repo::ID_ENTITY));

		// Assert that the content of the entity is as expected.
		$this->assertEquals($entity::BUFFER, Snapshot::buffer($entity));

		// Assert that an unranged entity cannot be given a range.
		try {
			Snapshot::buffer($entity, 0, 0);
		} catch (UnsupportedRangeError $error) {
			// Do nothing (implicitly assign the exception to $error).
		}

		$this->assertNotNull($error);

		$this->assertNull($repo->fetch($path));

		$repo->store($path, $entity);

		// Assert that a file can be fetched from the file system.
		$file = $repo->fetch($path);
		$this->assertEquals($entity::BUFFER, file_get_contents($path));
		$this->assertEquals($entity::BUFFER, Snapshot::buffer($file));
		$this->assertEquals(substr($entity::BUFFER, 0, 5), Snapshot::buffer($file, 0, 5));
		$this->assertEquals(substr($entity::BUFFER, 8, 5), Snapshot::buffer($file, 8, 5));

		// Assert that a previously fetched entity is fetched from the cache.
		$cached = $repo->fetch($path);
		$this->assertNotSame($file, $cached);
		$this->assertLessThanOrEqual($cached->modified(), $file->modified());

		// Assert that an unmodified entity is not cached.
		$this->assertFalse($repo->cache($path));

		// Assert that a modified entity is not fetched if not recached.
		$this->assertSame($cached, $repo->fetch($path));

		// Assert that a recached entity is fetched.
		clearstatcache();
		sleep(1); file_put_contents($path, self::BUFFER);
		$this->assertEquals(self::BUFFER, file_get_contents($path));
		$this->assertTrue($repo->cache($path));

		$modified = $repo->fetch($path);
		$this->assertEquals(self::BUFFER, Snapshot::buffer($modified));
		$this->assertEquals(substr(self::BUFFER, 0, 7), Snapshot::buffer($modified, 0, 7));
		$this->assertEquals(substr(self::BUFFER, 10, 6), Snapshot::buffer($modified, 10, 6));
	}

	private function repository(Entity $entity): Repository
	{
		return new class($entity) implements Repository {

			const ID_ENTITY = 'entity';

			use Cache {
				Cache::fetch as actual;
				Cache::cache as public;
			}

			private $entity;

			private $repository;

			public function __construct(Entity $entity)
			{
				$this->entity = $entity;
			}

			public function fetch(string $id): ?Entity
			{
				$entity = null;

				switch ($id) {
					case self::ID_ENTITY:
						$entity = $this->entity;
						break;
					default:
						$entity = $this->actual($id);
						break;
				}

				return $entity;
			}

			protected function repository(): Repository
			{
				if (is_null($this->repository)) {

					$this->repository = new class implements Repository {
						use FileSystem;
					};
				}

				return $this->repository;
			}
		};
	}

	private function unranged(): Entity
	{
		return new class implements Entity {

			const BUFFER = 'I am unranged.';

			use Touchable, Unranged {
				Touchable::touch as public __construct;
			}

			protected function passthru(): void
			{
				echo self::BUFFER;
			}
		};
	}
}
